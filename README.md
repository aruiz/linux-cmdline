# linux-cmdline: A Linux kernel cmdline parser and manipulation API

This crate aims to provide an API to parse, manipulate and writeback Linux kernel cmdline strings.